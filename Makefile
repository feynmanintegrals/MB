depend:
	gfortran --version
	cd extra/Cuba/ && ./configure --prefix=`pwd`/../../usr/
	export ARFLAGS=rvU && $(MAKE) -C ./extra/Cuba lib
	export ARFLAGS=rvU && $(MAKE) -C ./extra/Cuba install
	$(MAKE) -C ./extra/libgamma
	cp -f extra/libgamma/libgamma.a usr/lib/
	math < test.m

clean:
	if [ -f extra/Cuba/makefile ]; then $(MAKE) -C ./extra/Cuba distclean; fi;
	rm -rf usr/include/*
	rm -rf usr/lib/*
	rm -rf usr/share/*
	rm -rf usr/lib64/*
	rm -rf usr/bin/*
	rm -rf extra/libgamma/*.o
	rm -rf extra/libgamma/libgamma.a

.PHONY: test testC
test:
	math < test.m

testC:
	echo "Get[\"tests/MBcreateTests.m\"]; Quit[];" | math
