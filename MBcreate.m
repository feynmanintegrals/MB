If[Options[MBcreate] === {},
    Options[MBcreate] = {
        "XVar" -> x,
        "ZVar" -> z,
        "EpVar" -> ep,
        "d0" -> 4,
        "Verbose" -> Automatic,
        "SplitExtraVars" -> True
    }
];

MBcreate::usage = "Creates MB representaion. Input is either {U, F, loops} or {gammas, monom, {{function, exponent},...}, UsedDelta}. The possible options include XVar, ZVar, EpVar, d0 and Verbose which can have options False (minimal), Automatic (middle, default) and True (maximum)";

BeginPackage["MB`"];

Begin["`Private`"];

MBcreate = Global`MBcreate;

SeparateMonom::usage = "SeparateMonom[{gammas,monom,functions,UsedDelta}, options] splits possible monomials from functions to monom";
SeparateMonom[{gammas_,monom2_,functions2_,UsedDelta_}, options:OptionsPattern[MBcreate]]:=Module[{temp, c, i, j, monom, monoms, functions},
    {monom,functions}={monom2,functions2};
    functions=Flatten[(
        c=##[[2]];
        temp=FactorList[##[[1]]];
        If[Length[temp] > 1,
            For[i = 1, i <= Length[temp], ++i,
                If[And[IntegerQ[temp[[i, 1]]], temp[[i, 1]] < 0],
                    If[temp[[i,2]] =!= 1,
                        Print["Strange FactorList"];
                        Print[InputForm[{monom2, functions2}]];
                        Abort[];
                    ];
                    For[j = 1, j <= Length[temp], j++,
                        If[i == j, Continue[]];
                        If[temp[[j,1]] == 1, Continue[]];
                        If[Head[temp[[j,1]]] === OptionValue["XVar"], Continue[]];
                        (*If[And @@ ((## > 0) & /@ List @@ temp[[j,1]] /. OptionValue["XVar"][_] -> 1), Continue[]];*) (*that would make a pure negative after multiplication*)
                        If[temp[[j,2]] == 1,
                            temp[[j,1]] *= temp[[i,1]];
                            temp[[i,1]] = 1;
                            Break[];
                        ]
                    ];
                    If[j > Length[temp],
                        Print["Strange FactorList"];
                        Print[InputForm[{monom2, functions2}]];
                        Print[##[[1]]];
                        Abort[];
                    ];
                ];
            ];
            temp=DeleteCases[temp,{1,_}];
            temp={##[[1]],##[[2]]*c}&/@temp;
        ];
        temp
    )&/@functions,1];
    functions={Expand[##[[1]]],##[[2]]}&/@functions;
    monoms=Select[functions,(Head[##[[1]]]=!=Plus)&];
    monom*=(Times@@((##[[1]]^##[[2]])&/@monoms));
    functions=Select[functions,(Head[##[[1]]]===Plus)&];
    functions=Reap[Sow[##[[2]], ##[[1]]] & /@ functions, _, {#1, Expand[Plus @@ #2]} &][[2]];
    functions=DeleteCases[functions,{_,0}];
    {gammas,monom,functions,UsedDelta}
]

SortFunctions[functions_i, options:OptionsPattern[MBcreate]] := Reverse[SortBy[functions, {Exponent[##[[1]] /. OptionValue["XVar"][_] -> OptionValue["XVar"], OptionValue["XVar"]] &, Length[##[[1]]] &}]]

MakeStep::usage = "MakeStep[{gammas,monom,functions,UsedDelta}, i, options] introduces MB by x[i] if needed and integrates by x[i]";
MakeStep[{gammas2_,monom2_,functions2_,UsedDelta_}, i_, options:OptionsPattern[MBcreate]]:=Module[{temp, monom, functions, gammas, ll, j, u, var, powers, bad, newfunctions, a, l, alpha, beta},
    var=OptionValue["XVar"][i];
    monom=monom2;
    functions=functions2;
    gammas=gammas2;
    If[Not[MemberQ[Variables[functions],var]],
        Return[{{gammas, monom, functions, UsedDelta}, 0}]
    ];
    powers={Exponent[##[[1]],var]}&/@functions;
    bad=Length[Select[powers,(##[[1]]!=0)&]];

    ll = 1;
    l=Length[Union[Cases[{monom2,functions2,gammas2},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    newfunctions={};
    If[bad>1,
        For[j=Length[functions],j>0,j--,
            If[powers[[j]][[1]]!=0,
                temp=Table[{Coefficient[functions[[j]][[1]],var,u],If[u===powers[[j]][[1]],functions[[j]][[2]],0]},{u,0,powers[[j]][[1]]}];
                a=temp[[Length[temp]]][[2]];
                For[u=1,u<Length[temp],u++,
                    temp[[u]][[2]]+=OptionValue["ZVar"][l];
                    gammas*=(Gamma[-OptionValue["ZVar"][l]]);
                    temp[[Length[temp]]][[2]]-=OptionValue["ZVar"][l];
                    l++;
                    ll++;
                ];
                For[u=1,u<=Length[temp],u++,
                    monom*=(var^(temp[[u]][[2]]*(u-1)));
                ];
                gammas*=(Gamma[Expand[-temp[[Length[temp]]][[2]]]]/Gamma[-a]);
                newfunctions=Join[temp,newfunctions];
                functions=Delete[functions,j];
                bad--;
            ];
            If[bad==1,Break[]];
        ]
    ];
    functions=Join[functions,newfunctions];
    (*now performing integration for var*)
    powers={Exponent[##[[1]],var]}&/@functions;
    For[j=1,j<=Length[functions],j++,
        If[powers[[j]][[1]]!=0,Break[]];
    ];
    If[powers[[j]][[1]]>1,Return[{{gammas2, monom2, functions2, UsedDelta}, Infinity}]];
    alpha=Exponent[monom,var];
    beta=functions[[j]][[2]];
    newfunctions={{Coefficient[functions[[j]][[1]],var,0],beta+alpha+1},{Coefficient[functions[[j]][[1]],var,1],-alpha-1}};
    gammas*=(Gamma[alpha+1]*Gamma[-beta-alpha-1]/Gamma[-beta]);
    functions=Delete[functions,j];
    functions=Join[newfunctions,functions];
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    monom=monom/.var->1;
    Return[{{gammas, monom, functions, UsedDelta},ll}];
]

MBinit[{U_,V_,h_}, options:OptionsPattern[MBcreate]] := Module[{temp, vars, n, i, var, c, newfunctions, functions, monom, monoms, u, p1, p2, gammas},
    vars=Sort[Cases[Variables[{U,V}],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    {p1,p2}=Expand[{n-(h+1)(OptionValue["d0"]/2-OptionValue["EpVar"]),-(n-h(OptionValue["d0"]/2-OptionValue["EpVar"]))}];
    functions={{U,p1},{V,p2}};
    monom=1;
    gammas= Gamma[Expand[n+ h*(OptionValue["EpVar"]-2)]] ;
    {gammas,monom,functions,False}
]

MBauto::usage = "MBauto[{gammas,monom,functions,UsedDelta}, options] performs all possible simplifications which do not require an introduction of an extra MB";
MBauto[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, temp, vars, n, i, var, c, newfunctions, monoms, l, u, beta, monom3, functions3, gammas3, PairsSearch, j, rule, alpha, xi, eta, ll, k, UsedDelta3},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[{monom, functions}],OptionValue["XVar"][_]]];
    If[Length[vars]==0,
        Return[{gammas,monom,functions,UsedDelta}]
    ];
    n=Last[vars][[1]];
    Label[PairsSearch];
    For[i=1,i<=n,i++,
        If[Not[MemberQ[Variables[ExpandAll[functions]],OptionValue["XVar"][i]]],
            Continue[]
        ];
        For[j=i+1,j<=n,j++,
           If[Not[MemberQ[Variables[ExpandAll[functions]],OptionValue["XVar"][j]]],Continue[]];
            rule=OptionValue["XVar"][i]->-OptionValue["XVar"][j];
            If[Not[MemberQ[Variables[ExpandAll[functions/.rule]],OptionValue["XVar"][j]]],   (* simple pair replacement - integrating by xi *)
                If[OptionValue["Verbose"] =!= False,
                    Print["Sum of two variables only in one factor, integrating: ",{i,j}];
                ];
                {gammas3,monom3,functions3,UsedDelta3}={gammas,monom,functions,UsedDelta};
                alpha=Exponent[monom,OptionValue["XVar"][i]];
                beta=Exponent[monom,OptionValue["XVar"][j]];
                gammas*=(Gamma[alpha+1]Gamma[beta+1]/Gamma[alpha+beta+2]); (* integral by xi *)
                monom=monom/.(OptionValue["XVar"][i]->OptionValue["XVar"][j]); (* eta *)
                monom*=OptionValue["XVar"][j];
                functions=functions/.(OptionValue["XVar"][i]->0);
                {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
                monom = PowerExpand[monom];
                functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                If[OptionValue["Verbose"] == True,
                    Print[{gammas,monom,functions,UsedDelta}];
                ];
                If[Length[Cases[Variables[functions],OptionValue["XVar"][_]]]==0,
                    If[OptionValue["Verbose"] =!= False,
                        Print["Stepping back, using delta"];
                    ];
                    {gammas,monom,functions,UsedDelta} = {gammas3,monom3,functions3,True}/.OptionValue["XVar"][i]->1;
                    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                    If[OptionValue["Verbose"] === True,
                        Print[{gammas,monom,functions,UsedDelta}];
                    ];
                    GoTo[SingleSearch];
                ];
                Goto[PairsSearch];
            ,
                {functions3,gammas3}=Expand[{functions,gammas}/.{OptionValue["XVar"][i]->eta*xi,OptionValue["XVar"][j]->eta*(1-xi)}];
                {functions3,gammas3}=Expand[{functions3,gammas3}/.{xi->OptionValue["XVar"][i],eta->OptionValue["XVar"][j]}];
                beta=Exponent[monom,OptionValue["XVar"][j]];
                monom3=PowerExpand[OptionValue["XVar"][j]*(monom/.{OptionValue["XVar"][i]->OptionValue["XVar"][i]*OptionValue["XVar"][j]})];
                {monom3,functions3} = Take[SeparateMonom[{gammas,monom3,functions3,UsedDelta}, options], {2, 3}];
                monom3 = PowerExpand[monom3];
                {{gammas3,monom3,functions3,UsedDelta3}, ll} = MakeStep[{gammas3,monom3,functions3,UsedDelta3}, j, options];
                If[ll===1,  (* advanced pair replacement - integration by  eta, then xi *)
                (* !!!! should be a special case for returned 0*)
                    For[k=1,k<=Length[functions3],k++,
                        If[Not[MemberQ[Variables[functions3[[k]][[1]]],OptionValue["XVar"][i]]],Continue[]];
                        If[TrueQ[NumberQ[Factor[functions3[[k]][[1]]/(1-OptionValue["XVar"][i])]]],
                            beta+=functions3[[k]][[2]];
                        ,
                            beta=Infinity;
                            Break[];
                        ];
                    ];
                    If[beta=!=Infinity,
                        alpha=Exponent[monom3,OptionValue["XVar"][i]];
                        gammas3*=(Gamma[alpha+1]Gamma[beta+1]/Gamma[alpha+beta+2]); (* integral by xi *)
                        monom3=monom3/.(OptionValue["XVar"][i]->1); (* eta *)
                        functions3=functions3/.(OptionValue["XVar"][i]->0);
                        {monom3,functions3} = Take[SeparateMonom[{gammas,monom3,functions3,UsedDelta}, options], {2, 3}];
                        monom3 = PowerExpand[monom3];
                        If[OptionValue["Verbose"] =!= False,
                            Print["{x[i] -> eta*xi, x[j] -> eta*(1 - xi)} /. {xi -> x[i], eta -> x[j]}, integrating by x[j]: ",{i,j}];
                        ];
                        {gammas,monom,functions}={gammas3,monom3,functions3};
                        functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                        If[OptionValue["Verbose"] == True,
                            Print[{gammas,monom,functions,UsedDelta}];
                        ];
                        Goto[PairsSearch];
                    ]
                ];
            ];
        ];
    ];
    Label[SingleSearch];
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    If[And[Length[Union[Cases[Variables[{monom,functions}],OptionValue["XVar"][_]]]]===1,Not[UsedDelta]],
        var=Cases[Variables[{monom,functions}],OptionValue["XVar"][_]][[1]];
        monom=monom/.var->1;
        functions=functions/.var->1;
        If[OptionValue["Verbose"] =!= False,
            Print["Using delta for ", var];
        ];
        functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
        If[OptionValue["Verbose"] == True,
            Print[{gammas,monom,functions,UsedDelta}];
        ];
        Return[{gammas,monom,functions,UsedDelta}];
    ];
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    For[j=1,j<=n,j++,
        If[Not[MemberQ[vars,OptionValue["XVar"][j]]],
            Continue[]
        ];
        {{gammas3,monom3,functions3,UsedDelta}, ll} = MakeStep[{gammas,monom,functions,UsedDelta}, j, options];
        If[ll==1,
            (*Print[{gammas,monom,functions,UsedDelta}];*)
            If[OptionValue["Verbose"] =!= False,
                Print["Integration: ",j];
            ];
            {monom,functions,gammas}={monom3,functions3,gammas3};
            monom = PowerExpand[monom];
            functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
            If[OptionValue["Verbose"] == True,
                Print[{gammas,monom,functions,UsedDelta}];
            ];
            If[UsedDelta,GoTo[SingleSearch],Goto[PairsSearch]];
        ];
    ];
    If[And[Length[Union[Cases[Variables[{monom,functions}],OptionValue["XVar"][_]]]]===1,Not[UsedDelta]],
        var=Cases[Variables[{monom,functions}],OptionValue["XVar"][_]][[1]];
        monom=monom/.var->1;
        functions=functions/.var->1;
        If[OptionValue["Verbose"] =!= False,
            Print["Using delta for ", var];
        ];
        functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
        If[OptionValue["Verbose"] == True,
            Print[{gammas,monom,functions,UsedDelta}];
        ];
    ];
    {gammas,monom,functions,UsedDelta}

]

MBsplitExtraVars::usage = "MBsplitExtraVars[{gammas,monom,functions,UsedDelta}, options] splits with MB for all extra variables (kinematic invariants)";
MBsplitExtraVars[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, vars, n, l, extraVars, var, j, i, zeroTerm, coeff, restart},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    Label[restart];
    l=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    extraVars = Complement[Variables[functions], vars];
    For[j = 1, j<=Length[extraVars], ++j,
        var = extraVars[[j]];
        For[i = 1, i<=Length[functions], ++i,
            If[Exponent[functions[[i,1]], var] == 1,
                zeroTerm = functions[[i,1]]/.var->0;
                coeff = (functions[[i,1]] - zeroTerm)/var;
                If[OptionValue["Verbose"] =!= False,
                    Print["Splitting f[",i,"] = c1 * ",var," + c2"];
                ];
                (*
                    functions[[i,1]] = zeroTerm + coeff*var;
                    (zeroTerm + coeff*var)^power -> Gamma[-z]*Gamma[-power+z]/Gamma[-power] * coeff^z * var^z * zeroTerm^(power-z)
                *)
                gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-functions[[i,2]]+OptionValue["ZVar"][l]]/Gamma[-functions[[i,2]]]);
                AppendTo[functions, {coeff, OptionValue["ZVar"][l]}];
                functions[[i, 1]] = zeroTerm;
                functions[[i, 2]] -= OptionValue["ZVar"][l];
                monom*=var^OptionValue["ZVar"][l];
                {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
                monom = PowerExpand[monom];
                functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                If[OptionValue["Verbose"] == True,
                    Print[{gammas,monom,functions,UsedDelta}];
                ];
                Goto[restart];
            ];
        ]
    ];
    {gammas,monom,functions,UsedDelta}
]

MBpairStep::usage = "MBpairStep[{gammas,monom,functions,UsedDelta}, {i, j} options] performs a trick of replacing x[i]->eta*ksi, x[j]->eta(1-ksi), integration by eta splitting with MB and integration";
MBpairStep[{gammas2_,monom2_,functions2_,UsedDelta2_}, {i_,j_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, temp, vars, n, var, c, newfunctions, monoms, l, ll, u, k, part0, part1, part2, xx, alpha, beta,eta1,eta2,xi,pow},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    l=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    {gammas, monom, functions, UsedDelta} = {gammas, monom, functions, UsedDelta} /. {OptionValue["XVar"][i] -> eta1*xi, OptionValue["XVar"][j] -> eta2*xi} /. {eta1 -> OptionValue["XVar"][i],  eta2 -> OptionValue["XVar"][j], xi -> OptionValue["XVar"][n+1]};
    monom*=OptionValue["XVar"][n+1]; (* Jacobian *)
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    If[OptionValue["Verbose"] =!= False,
        Print["Integrating by x[",i,"]+x[",j,"]->x[", n+1,"]"];
    ];
    {{gammas,monom,functions,UsedDelta}, xx} = MakeStep[{gammas,monom,functions,UsedDelta}, n+1, options];
    l=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
    If[OptionValue["Verbose"] == True,
        Print[{gammas,monom,functions,UsedDelta}];
    ];
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    For[k=1,k<=Length[functions],k++,
        If[MemberQ[Variables[Expand[functions[[k]][[1]]/.(OptionValue["XVar"][i]->-OptionValue["XVar"][j])]],OptionValue["XVar"][j]],
            Break[];
        ];
    ];
    If[k > Length[functions],
        If[OptionValue["Verbose"] == False,
            Return[False];
        ];
        Print["Strange MBpairStep 2"];
        Print[{gammas2,monom2,functions2,UsedDelta2}];
        Abort[];
    ];
    part1 = Coefficient[functions[[k,1]], OptionValue["XVar"][i]];
    part2 = Coefficient[functions[[k,1]], OptionValue["XVar"][j]];
    If[Not[Expand[functions[[k,1]] - part1 * OptionValue["XVar"][i] - part2 * OptionValue["XVar"][j]] === 0],
        If[OptionValue["Verbose"] == False,
            Return[False];
        ];
        Print["Strange MBpairStep"];
        Print[{gammas2,monom2,functions2,UsedDelta2}];
        Print[functions[[k]]];
        Print[Expand[functions[[k,1]] - part1 * OptionValue["XVar"][i] - part2 * OptionValue["XVar"][j]]];
        Abort[];
    ];
    If[OptionValue["Verbose"] =!= False,
        Print["Splitting function ", k, " by x[",i,"] and x[",j,"] = 1 - x[", i,"]"];
    ];
    pow = functions[[k,2]];
        (*(zeroTerm + coeff*var)^power -> Gamma[-z]*Gamma[-power+z]/Gamma[-power] * coeff^z * var^z * zeroTerm^(power-z)*)
    functions = Join[Delete[functions, k], {{OptionValue["XVar"][i] * part1, OptionValue["ZVar"][l]}, {OptionValue["XVar"][j] * part2, pow - OptionValue["ZVar"][l]}}];
    gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-pow+OptionValue["ZVar"][l]]/Gamma[-pow]);
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    
    pow = Exponent[monom, OptionValue["XVar"][j]];
    monom = monom/OptionValue["XVar"][j]^pow;
    {gammas, monom, functions, UsedDelta} = {gammas, monom, functions, UsedDelta} /. {OptionValue["XVar"][j] -> 1 - OptionValue["XVar"][i]};
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
    If[OptionValue["Verbose"] == True,
        Print[{gammas,monom*(1-OptionValue["XVar"][j])^pow,functions,UsedDelta}];
    ];
    If[MemberQ[Variables[functions], OptionValue["XVar"][i]],
        If[OptionValue["Verbose"] == False,
            Return[False];
        ];
        Print["Strange MBpairStep 3"];
        Print[{gammas2,monom2,functions2,UsedDelta2}];
        Print[{i,j}];
        Print[functions[[k]]];
        Print[Expand[functions[[k,1]] - part1 * OptionValue["XVar"][i] - part2 * OptionValue["XVar"][j]]];
        Abort[];
    ];
    If[OptionValue["Verbose"] =!= False,
        Print["Integrating by x[",i,"] to 1"];
    ];
    alpha = Exponent[monom, OptionValue["XVar"][i]];
    gammas*=(Gamma[alpha+1]*Gamma[pow+1]/Gamma[alpha+pow+2]);
    monom = monom/. {OptionValue["XVar"][i] -> 1};
    monom = PowerExpand[monom];
    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
    If[OptionValue["Verbose"] == True,
        Print[{gammas,monom,functions,UsedDelta}];
    ];
    MBauto[{gammas,monom,functions,UsedDelta}, options]
]

MBsuggestVar::usage = "MBsuggestVar[{gammas,monom,functions,UsedDelta}, {i, j} options] suggests variables suitable for MakeStep";
MBsuggestVar[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, temp, vars, n, i, var, c, newfunctions, monoms, l, u, j, BAD, monom3, functions3, gammas3, ll, H},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    (*searching for the best variable*)

    BAD=Reap[
        For[j=1,j<=n,j++,
            If[Not[MemberQ[vars,OptionValue["XVar"][j]]],Continue[]];
            {{gammas3,monom3,functions3,UsedDelta}, ll} = MakeStep[{gammas,monom,functions,UsedDelta}, j, options];
            Sow[H[j,ll,Length[functions3],Plus @@ Length /@ First /@ functions3]];
        ];
    ][[2]][[1]];
    BAD=Sort[BAD,(If[#1[[2]]<#2[[2]],True,If[#1[[2]]>#2[[2]],False,If[#1[[3]]>#2[[3]],False,#1[[4]]<#2[[4]]]]])&];
    BAD=Select[BAD,And[##[[2]]==BAD[[1]][[2]],##[[3]]==BAD[[1]][[3]],##[[4]]==BAD[[1]][[4]]]&];
    BAD
]

MBtrySplitToAnotherFunction::usage = "MBtrySplitToAnotherFunction[{gammas,monom,functions,UsedDelta}, {i, j} options] tries MB splitting based on one function being similar to another";
MBtrySplitToAnotherFunction[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, i, j, ll, vars, n, var, powers, bad, k, coeff, kk, l, zeroTerm, len, tempFunctions},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    l=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    For[i=1,i<=n,i++,
        var = OptionValue["XVar"][i];
        If[Not[MemberQ[vars,var]],Continue[]];
        For[k = 1, k <= Length[functions], ++k,
            If[And[IntegerQ[functions[[k,2]]], functions[[k,2]] > 0], Continue[]];
            If[Exponent[functions[[k,1]], var] == 1,
                For[kk = 1, kk <= Length[functions], ++kk,
                    If[kk == k, Continue[]];
                    If[Exponent[functions[[kk,1]], var] =!= 0, Continue[]];
                    zeroTerm = Expand[functions[[k,1]] - var * functions[[kk,1]]];
                    len = If[Head[zeroTerm] === Plus, Length[zeroTerm], 1];
                    If[Length[functions[[k,1]]] =!= len + Length[functions[[kk,1]]], Continue[]]; 
                    If[Not[Or[
                            (zeroTerm /. var->0) === 0
                        ]]
                    ,
                        Continue[];
                    ];
                    coeff = functions[[kk,1]];
                    If[OptionValue["Verbose"] =!= False,
                        Print["Splitting f[",k,"] = x[",i,"] * f[",kk,"] + c (c does not depend on x[",i,"])"];
                    ];
                    (*
                        functions[[k,1]] = zeroTerm + coeff*var;
                        (zeroTerm + coeff*var)^power -> Gamma[-z]*Gamma[-power+z]/Gamma[-power] * coeff^z * var^z * zeroTerm^(power-z)
                        functions[[kk,1]] = coeff;
                    *)
                    gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-functions[[k,2]]+OptionValue["ZVar"][l]]/Gamma[-functions[[k,2]]]);
                    functions[[kk, 2]] += OptionValue["ZVar"][l];
                    functions[[k, 1]] = zeroTerm;
                    functions[[k, 2]] -= OptionValue["ZVar"][l];
                    monom*=var^OptionValue["ZVar"][l];
                    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
                    monom = PowerExpand[monom];
                    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                    If[OptionValue["Verbose"] == True,
                        Print[{gammas,monom,functions,UsedDelta}];
                    ];
                    Return[MBauto[{gammas,monom,functions,UsedDelta}, options]];
                ];
            ]
        ]
    ];
    For[i=1,i<=n,i++,
        var = OptionValue["XVar"][i];
        If[Not[MemberQ[vars,var]],Continue[]];
        For[k = 1, k <= 1, ++k,
            If[And[IntegerQ[functions[[k, 2]]], functions[[k, 2]] > 0], Continue[]];
            If[Exponent[functions[[k, 1]], var] == 1,
                zeroTerm = functions[[k, 1]] /. var->0;
                coeff = Coefficient[functions[[k,1]], var];
                If[IntegerQ[coeff], Continue[]];
                tempFunctions = Append[functions, {coeff, OptionValue["ZVar"][l]}];
                tempFunctions[[k, 1]] = zeroTerm;
                tempFunctions[[k, 2]] -= OptionValue["ZVar"][l];
                tempFunctions = SeparateMonom[{gammas,monom,tempFunctions,UsedDelta}, options][[3]];
                If[Length[tempFunctions] < Length[functions],
                    If[OptionValue["Verbose"] =!= False,
                        Print["Splitting f[",k,"] as x[",i,"] * c1 + c2 (c2 does not depend on x[",i,"], c1 and c2 are factored into monomial and existing)"];
                    ];
                    gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-functions[[k,2]]+OptionValue["ZVar"][l]]/Gamma[-functions[[k,2]]]);
                    functions = Append[functions, {coeff, OptionValue["ZVar"][l]}];
                    functions[[k, 1]] = zeroTerm;
                    functions[[k, 2]] -= OptionValue["ZVar"][l];
                    monom*=var^OptionValue["ZVar"][l];
                    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
                    monom = PowerExpand[monom];
                    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                    If[OptionValue["Verbose"] == True,
                        Print[{gammas,monom,functions,UsedDelta}];
                    ];
                    Return[MBauto[{gammas,monom,functions,UsedDelta}, options]];
                ];
            ];
        ];
    ];
    For[i=1,i<=n,i++,
        var = OptionValue["XVar"][i];
        If[Not[MemberQ[vars,var]],Continue[]];
        For[k = 1, k <= Length[functions], ++k,
            If[And[IntegerQ[functions[[k,2]]], functions[[k,2]] > 0], Continue[]];
            If[Exponent[functions[[k,1]], var] == 1,
                For[kk = 1, kk <= Length[functions], ++kk,
                    If[kk == k, Continue[]];
                    If[Exponent[functions[[kk,1]], var] =!= 1, Continue[]];
                    coeff = functions[[k,1]] - functions[[kk,1]];
                    len = If[Head[coeff] === Plus, Length[coeff], 1];
                    If[Length[functions[[k,1]]] =!= len + Length[functions[[kk,1]]], Continue[]]; 
                    If[(coeff /. var->0) =!= 0, Continue[]];
                    coeff = coeff / var;
                    zeroTerm = functions[[kk, 1]];
                    If[OptionValue["Verbose"] =!= False,
                        Print["Splitting f[",k,"] = x[",i,"] * c + f[",kk,"]"];
                    ];
                    (*
                        functions[[k,1]] = zeroTerm + coeff*var;
                        (zeroTerm + coeff*var)^power -> Gamma[-z]*Gamma[-power+z]/Gamma[-power] * coeff^z * var^z * zeroTerm^(power-z)
                        functions[[kk,1]] = zeroTerm;
                    *)
                    gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-functions[[k,2]]+OptionValue["ZVar"][l]]/Gamma[-functions[[k,2]]]);
                    functions[[kk, 2]] += (functions[[k,2]] - OptionValue["ZVar"][l]);
                    functions[[k, 1]] = coeff;
                    functions[[k, 2]] = OptionValue["ZVar"][l];
                    monom*=var^OptionValue["ZVar"][l];
                    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
                    monom = PowerExpand[monom];
                    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                    If[OptionValue["Verbose"] == True,
                        Print[{gammas,monom,functions,UsedDelta}];
                    ];
                    Return[MBauto[{gammas,monom,functions,UsedDelta}, options]];
                ]
            ];
        ];
    ];
    For[i=1,i<=n,i++,
        var = OptionValue["XVar"][i];
        If[Not[MemberQ[vars,var]],Continue[]];
        For[k = 1, k <= 1, ++k,
            If[And[IntegerQ[functions[[k,2]]], functions[[k,2]] > 0], Continue[]];
            If[Exponent[functions[[k,1]], var] == 1,
                For[kk = 1, kk <= Length[functions], ++kk,
                    If[kk == k, Continue[]];
                    If[Exponent[functions[[kk,1]], var] =!= 0, Continue[]];
                    zeroTerm = Expand[functions[[k,1]] - var * functions[[kk,1]]];
                    len = If[Head[zeroTerm] === Plus, Length[zeroTerm], 1];
                    If[Length[functions[[k,1]]] =!= len + Length[functions[[kk,1]]], Continue[]];
                    If[Not[Or[
                            Length[FactorList[zeroTerm]] > 2
                        ]]
                    ,
                        Continue[];
                    ];
                    coeff = functions[[kk,1]];
                    If[OptionValue["Verbose"] =!= False,
                        Print["Splitting f[",k,"] = x[",i,"] * f[",kk,"] + c (c is factored)"];
                    ];
                    (*
                        functions[[k,1]] = zeroTerm + coeff*var;
                        (zeroTerm + coeff*var)^power -> Gamma[-z]*Gamma[-power+z]/Gamma[-power] * coeff^z * var^z * zeroTerm^(power-z)
                        functions[[kk,1]] = coeff;
                    *)
                    gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-functions[[k,2]]+OptionValue["ZVar"][l]]/Gamma[-functions[[k,2]]]);
                    functions[[kk, 2]] += OptionValue["ZVar"][l];
                    functions[[k, 1]] = zeroTerm;
                    functions[[k, 2]] -= OptionValue["ZVar"][l];
                    monom*=var^OptionValue["ZVar"][l];
                    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
                    monom = PowerExpand[monom];
                    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
                    If[OptionValue["Verbose"] == True,
                        Print[{gammas,monom,functions,UsedDelta}];
                    ];
                    Return[MBauto[{gammas,monom,functions,UsedDelta}, options]];
                ];
            ]
        ]
    ];
    Return[False];
];

MBtrySplitToSimilarFunction::usage = "MBtrySplitToSimilarFunction[{gammas,monom,functions,UsedDelta}, {i, j} options] tries MB splitting based on one function being similar to another";
MBtrySplitToSimilarFunction[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, i, j, ll, vars, n, var, powers, bad, k, coeff, kk, l, zeroTerm, len},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    l=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    For[k = 1, k <= Length[functions], ++k,
        For[kk = 1, kk <= Length[functions], ++kk,
            If[k == kk, Continue[]];
            If[Length[functions[[k,1]]] <= 4, Continue[]];
            If[Length[functions[[kk,1]]] <= 4, Continue[]];
            If[Length[Complement[List@@functions[[k,1]],List@@functions[[kk,1]]]] =!= 1, Continue[]];
            If[Length[Complement[List@@functions[[kk,1]],List@@functions[[k,1]]]] =!= 1, Continue[]];
            If[OptionValue["Verbose"] =!= False,
                Print["Splitting f[",k,"] = f[",kk,"] + difference of two monoms"];
            ];
            zeroTerm = Intersection[functions[[k,1]], functions[[kk,1]]];
            coeff = Expand[functions[[k,1]] - zeroTerm];
            (*
                functions[[k,1]] = zeroTerm + coeff;
                (zeroTerm + coeff)^power -> Gamma[-z]*Gamma[-power+z]/Gamma[-power] * coeff^z * zeroTerm^(power-z)
            *)
            gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-functions[[k,2]]+OptionValue["ZVar"][l]]/Gamma[-functions[[k,2]]]);
            AppendTo[functions, {coeff, OptionValue["ZVar"][l]}];
            functions[[k, 1]] = zeroTerm;
            functions[[k, 2]] -= OptionValue["ZVar"][l];
            {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
            monom = PowerExpand[monom];
            functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
            If[OptionValue["Verbose"] == True,
                Print[{gammas,monom,functions,UsedDelta}];
            ];
            Return[MBauto[{gammas,monom,functions,UsedDelta}, options]];
        ];
    ];

    Return[False];
]

MBsuggestPair::usage = "MBsuggestPair[{gammas,monom,functions,UsedDelta}, {i, j} options] suggests variables suitable for MBpairStep";
MBsuggestPair[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, temp, vars, n, i, var, c, newfunctions, monoms, l, u, BAD, j, H, min, eta, xi, res, ll},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];

    l=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    BAD=Reap[
        For[j=1,j<=n,j++,
            If[Not[MemberQ[vars,OptionValue["XVar"][j]]],
                Continue[]
            ];
            For[i=j+1,i<=n,i++,
                If[Not[MemberQ[vars,OptionValue["XVar"][i]]],
                    Continue[]
                ];
                functions = functions2/.{OptionValue["XVar"][i] -> eta*xi, OptionValue["XVar"][j] -> (1-eta)*xi} /. {eta -> OptionValue["XVar"][i], xi -> OptionValue["XVar"][j]};
                functions = SeparateMonom[{gammas,1,functions,UsedDelta}, options][[3]];
                If[And@@((Or[Not[MemberQ[Variables[Expand[##[[1]]]],OptionValue["XVar"][j]]],
                        Exponent[Expand[##[[1]]],OptionValue["XVar"][j]]<=1
                    ])&/@functions),
                    If[Length[DeleteCases[Exponent[Expand[##],OptionValue["XVar"][i]] & /@ First /@functions, 0]] > 1,
                        Continue[];
                    ];
                    If[Plus@@(Exponent[##, OptionValue["XVar"][j]] & /@ First /@functions) === 1,
                        Sow[H[j,i,Plus@@(Length /@ First /@functions)]]
                    ];
                    If[And[Max@@(Exponent[##, OptionValue["XVar"][j]] & /@ First /@functions) === 1, Plus@@(Exponent[##, OptionValue["XVar"][j]] & /@ First /@functions) === 2],
                        {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
                        res = MBpairStep[{gammas,monom,functions,UsedDelta}, {i, j}, Verbose->False];
                        If[res =!= False,
                            {gammas,monom,functions,UsedDelta} = res;
                            If[gammas =!= 0,
                                ll=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
                                If[Length[Variables[First/@functions]] + 3*(ll-l) < Length[Variables[First/@functions2]],
                                    Sow[H[j,i,ll-l]];
                                ];
                            ];
                        ];
                    ];
                ];
            ]
        ];
    ][[2]];
    If[BAD=={},
        Return[{}]
    ,
        BAD=BAD[[1]]
    ];
    BAD = SortBy[BAD, Last];
    BAD/.H->List
]

MBsuggestPair2::usage = "MBsuggestPair2[{gammas,monom,functions,UsedDelta}, {i, j} options] suggests variables suitable for MBpairSplit";
MBsuggestPair2[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, temp, vars, n, i, var, c, newfunctions, monoms, l, u, BAD, j, H, min, eta, xi},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];

    BAD=Reap[
        For[j=1,j<=n,j++,
            If[Not[MemberQ[vars,OptionValue["XVar"][j]]],
                Continue[]
            ];
            For[i=j+1,i<=n,i++,
                If[Not[MemberQ[vars,OptionValue["XVar"][i]]],
                    Continue[]
                ];
                functions = functions2/.{OptionValue["XVar"][i] -> -OptionValue["XVar"][j]};
                functions = functions /. {0, _} -> {1, 1};
                If[Plus@@((Exponent[##, OptionValue["XVar"][j]] ) & /@ First /@functions) === 1,
                    min = Position[Exponent[##, OptionValue["XVar"][j]] & /@ First /@functions, 1][[1,1]];
                    If[Exponent[functions2[[min,1]], OptionValue["XVar"][i]] + Exponent[functions2[[min,1]], OptionValue["XVar"][j]] === 1,
                        Sow[H[j,i,Plus@@(Length /@ First /@functions)]]
                    ];
                ];
            ]
        ];
    ][[2]];
    If[BAD=={},
        Return[{}]
    ,
        BAD=BAD[[1]]
    ];
    BAD = SortBy[BAD, Last];
    BAD/.H->List
]

MBpairSplit::usage = "MBpairSplit[{gammas,monom,functions,UsedDelta}, {i, j} options] splits a function depending on a single variable not the sum of two variables";
MBpairSplit[{gammas2_,monom2_,functions2_,UsedDelta2_}, {i_,j_}, options:OptionsPattern[MBcreate]]:=Module[{gammas, monom, functions, UsedDelta, temp, vars, n, var, c, newfunctions, monoms, l, ll, u, k, part0, part1, part2, xx, alpha, beta,eta1,eta2,xi,pow,zeroTerm, coeff},
    {gammas,monom,functions,UsedDelta}={gammas2,monom2,functions2,UsedDelta2};
    vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    l=Length[Union[Cases[{gammas,monom,functions,UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]]+1;
    For[k = 1, k<=Length[functions], ++k,
        If[Exponent[functions[[k, 1]]/.{OptionValue["XVar"][i] -> -OptionValue["XVar"][j]}, OptionValue["XVar"][j]] === 1,
            Break[];
        ];
    ];
    If[k > Length[functions],
        Print["Strange MBpairSplit"];
        Print[{gammas2,monom2,functions2,UsedDelta2}];
        Abort[];
    ];
    If[OptionValue["Verbose"] =!= False,
        Print["Splitting function ",k," as the only one not depending on sum of x[",i "] and x[", j,"]"];
    ];
    (*
        functions[[k,1]] = zeroTerm + coeff*var;
        (zeroTerm + coeff*var)^power -> Gamma[-z]*Gamma[-power+z]/Gamma[-power] * coeff^z * var^z * zeroTerm^(power-z)
        functions[[kk,1]] = coeff;
    *)
    If[MemberQ[Variables[functions[[k,1]]], OptionValue["XVar"][i]],
        var = OptionValue["XVar"][i]
    ,
        var = OptionValue["XVar"][j]
    ];
    zeroTerm = functions[[k,1]] /. {var->0};
    coeff = Coefficient[functions[[k,1]], var];
    gammas*=(Gamma[-OptionValue["ZVar"][l]]*Gamma[-functions[[k,2]]+OptionValue["ZVar"][l]]/Gamma[-functions[[k,2]]]);
    AppendTo[functions, {coeff, OptionValue["ZVar"][l]}];
    monom*=(var^OptionValue["ZVar"][l]);
    functions[[k, 1]] = zeroTerm;
    functions[[k, 2]] -= OptionValue["ZVar"][l];
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
    If[OptionValue["Verbose"] == True,
        Print[{gammas,monom,functions,UsedDelta}];
    ];
    Return[MBauto[{gammas,monom,functions,UsedDelta}, options]];
]

MBcreate[{U_,V_,h_}, options:OptionsPattern[MBcreate]] := Module[{data},
    vars=Sort[Cases[Variables[{U,V}],OptionValue["XVar"][_]]];
    n=Last[vars][[1]];
    {p1,p2}=Expand[{n-(h+1)(OptionValue["d0"]/2-OptionValue["EpVar"]),-(n-h(OptionValue["d0"]/2-OptionValue["EpVar"]))}];
    functions={{U,p1},{V,p2}};
    monom=1;
    gammas= Gamma[Expand[n+ h*(OptionValue["EpVar"]-2)]] ;
    MBcreate[{gammas,monom,functions,False}, options]
]

MBcreate[{gammas2_,monom2_,functions2_,UsedDelta2_}, options:OptionsPattern[MBcreate]] := Module[{gammas, monom, functions, UsedDelta, vars, l, temp},
    {gammas, monom, functions, UsedDelta} = {gammas2,monom2,functions2,UsedDelta2};
    {monom,functions} = Take[SeparateMonom[{gammas,monom,functions,UsedDelta}, options], {2, 3}];
    monom = PowerExpand[monom];
    functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
    If[OptionValue["Verbose"] === True,
        Print[{gammas, monom, functions, UsedDelta}];
    ];
    If[OptionValue["SplitExtraVars"] === True,
        {gammas, monom, functions, UsedDelta} = MBsplitExtraVars[{gammas, monom, functions, UsedDelta}, options];
    ];
    {gammas, monom, functions, UsedDelta} = MBauto[{gammas, monom, functions, UsedDelta}, options];
    While[Length[Cases[Variables[{gammas, monom, functions, UsedDelta}],OptionValue["XVar"][_]]]>0,
        vars=Sort[Cases[Variables[functions],OptionValue["XVar"][_]]];
        If[Length[vars] > 2,
            vars=MBsuggestPair[{gammas, monom, functions, UsedDelta}, options];
            If[Length[vars]>0,
                {gammas, monom, functions, UsedDelta}=MBpairStep[{gammas, monom, functions, UsedDelta}, Take[vars[[1]],2], options];
                Continue[];
            ];
            vars=MBsuggestPair2[{gammas, monom, functions, UsedDelta}, options];
            If[Length[vars]>0,
                {gammas, monom, functions, UsedDelta}=MBpairSplit[{gammas, monom, functions, UsedDelta}, Take[vars[[1]],2], options];
                Continue[];
            ];
        ];
        res = MBtrySplitToAnotherFunction[{gammas, monom, functions, UsedDelta}, options];
        If[res =!= False,
            {gammas, monom, functions, UsedDelta} = res;
            Continue[];
        ];
        res = MBtrySplitToSimilarFunction[{gammas, monom, functions, UsedDelta}, options];
        If[res =!= False,
            {gammas, monom, functions, UsedDelta} = res;
            Continue[];
        ];
        vars=MBsuggestVar[{gammas, monom, functions, UsedDelta}, options];
        If[Length[vars]>0,
            If[OptionValue["Verbose"] =!= False,
                Print["Introducing z, integrating: ",vars[[1,1]]];
            ];
            {{gammas,monom,functions,UsedDelta}, ll} = MakeStep[{gammas,monom,functions,UsedDelta}, vars[[1,1]], options];
            functions = Reverse[SortBy[functions, Length[First[##]] SortFunctions[functions, options]]];
            If[OptionValue["Verbose"] == True,
                Print[{gammas,monom,functions,UsedDelta}];
            ];
            If[ll===Infinity,
                Print["MakeStep returned nonsense"];
                Abort[]
            ];
            {gammas, monom, functions, UsedDelta}=MBauto[{gammas,monom,functions,UsedDelta}, options];
            Continue[];
        ];
    ];
    l=Length[Union[Cases[{gammas, monom, functions, UsedDelta},OptionValue["ZVar"][_],{0,Infinity}]]];
    Print["l = ",l];
    gammas*monom*Times@@((Power@@##)&/@functions)
]

End[];

EndPackage[];
