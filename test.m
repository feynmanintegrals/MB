Get["MB.m"];
int1 = MBint[-((
   mm^(-1 + z1 + z6) S^(-1 - z1 - z6)
     Gamma[1 - z1]^3 Gamma[z1]^2 Gamma[-z6]^3 Gamma[1 + z1 + z6])/(
   4 T Gamma[2 - 2 z1] Gamma[1 + z1]^2 Gamma[-2 z6])), {{ep ->
     0}, {z1 -> -(1/3), z6 -> -(1/3)}}];


MBintegrate[{int1}, {mm -> 1, S -> 0.3, T -> 0.7, U -> 0.4}, Debug->True] 
